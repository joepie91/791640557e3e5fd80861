# Promises reading list

This is a list of examples and articles, in roughly the order you should follow them, to show and explain how promises work and why you should use them. I'll probably add more things to this list over time.

This list primarily focuses on Bluebird, but the basic functionality should also work in ES6 Promises, and some examples are included on how to replicate Bluebird functionality with ES6 promises. You should still use Bluebird where possible, though - they are faster, less error-prone, and have more utilities.

I'm available for [tutoring and code review](http://cryto.net/~joepie91/code-review.html) :)

You may reuse all gists for any purpose under the [WTFPL](http://www.wtfpl.net/txt/copying/) / [CC0](https://creativecommons.org/publicdomain/zero/1.0/) (whichever you prefer).

## If you get stuck

I've made a [brief FAQ](https://gist.github.com/joepie91/4c3a10629a4263a522e3bc4839a28c83) of common questions that people have about Promises, and how to use them. If you don't understand something listed here, or you're wondering how to implement a specific requirement, chances are that it'll be answered in that FAQ.

## Compatibility

Bluebird will __not__ work correctly (in client-side code) in older browsers. If you need to support older browsers, and you're using Webpack or Browserify, you should use the [`es6-promise`](https://www.npmjs.com/package/es6-promise) module instead, and reimplement behaviour where necessary.

## Introduction

* Start reading [here](http://bluebirdjs.com/docs/why-promises.html), to understand why Promises matter.
* If it's not quite clear yet, [some code that uses callbacks, and its equivalent using Bluebird](https://gist.github.com/joepie91/c6aa1ee552dcac821d03).
* [A demonstration of how promise chains can be 'flattened'](https://gist.github.com/joepie91/211c8e99fb5a83b76079)

## Promise.try

Many guides and examples fail to demonstrate Promise.try, or to explain why it's important. [This article](http://cryto.net/~joepie91/blog/2016/05/11/what-is-promise-try-and-why-does-it-matter/) will explain it.

## Error handling

* [A quick introduction](https://gist.github.com/joepie91/c8d8cc4e6c2b57889446)
* An illustration of error bubbling: [step 1](https://gist.github.com/joepie91/2b62b735020e51b260abacaa133f48f0), [step 2](https://gist.github.com/joepie91/b0c8f9a9309f5398080eab84482d58a4)
* [Implementing 'fallback' values](https://gist.github.com/joepie91/f6a56acdae303e90e44a) (ie. defaults for when an asynchronous operation fails)
* [bluebird-tap-error](https://www.npmjs.com/package/bluebird-tap-error), a module for intercepting and looking at errors, without preventing propagation. Useful if you need to do the actual error handling elsewhere.
* [Handling errors in Express, using Promises](http://cryto.net/~joepie91/blog/2015/05/14/using-promises-bluebird-with-express/)

Many examples on the internet don't show this, but you should __always__ start a chain of promises with Promise.try, and if it is within a function or callback, you should always __return__ your promises chain. Not doing so, will result in less reliable error handling and various other issues (eg. code executing too soon).

## Promisifying

* [Promisifying functions and modules that use nodebacks](http://bluebirdjs.com/docs/api/promisification.html) (Node.js callbacks)
* [An example of manually promisifying an EventEmitter](https://gist.github.com/joepie91/3610c6e41bc654ccaadf)
* [Promisifying `fs.exists`](https://gist.github.com/joepie91/bbf495e044da043de2ba) (which is async, but doesn't follow the nodeback convention)

## Functional (map, filter, reduce)

* [Functional programming in Javascript: map, filter and reduce](http://cryto.net/~joepie91/blog/2015/05/04/functional-programming-in-javascript-map-filter-reduce/) (an introduction, not Bluebird-specific, but important to understand)
* [(Synchronous) examples of map, filter, and reduce in Bluebird](https://gist.github.com/joepie91/34742045a40f7c48430e)
* [Example of using map for retrieving a (remote) list of URLs with bhttp](https://gist.github.com/joepie91/4c125c45ee6c5ea0375f)

## Nesting

* [Example of retaining scope through nesting](https://gist.github.com/joepie91/7d22af310ef68de4f507)
* [Example of 'breaking out' of a chain through nesting](https://gist.github.com/joepie91/c5f99a18975df0bf2f98)
* [Example of a nested Promise.map](https://gist.github.com/joepie91/2aafe9e4830e0d0c8171)
* An example with increasing complexity, implementing an 'error-tolerant' Promise.map: [part 1](https://gist.github.com/joepie91/045a0238d0751cc7a72b), [part 2](https://gist.github.com/joepie91/11e36819dcca49f54348), [part 3](https://gist.github.com/joepie91/9593551b41f568a75b08)

## ES6 Promises

* [Documentation on MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise)
* [Promise.try using ES6 Promises](https://gist.github.com/joepie91/255250eeea8b94572a03)
* [Promise.delay using ES6 Promises](https://gist.github.com/joepie91/583db45f3a30552a7cd2)

## Odds and ends

Some potentially useful snippets:

* [Flattening an array of arrays, when using promises](https://gist.github.com/joepie91/ac1ee270c6a506405d5f)

You're unlikely to need any of these things, if you just stick with either Bluebird or ES6 promises:

* [How to test whether a Promises implementation handles callbacks correctly](https://gist.github.com/joepie91/48042173a6c9c4065399)
* [Why this matters.](https://gist.github.com/joepie91/98576de0fab7badec167)